 <div class="MortgageBanklist">
    <% _.forEach(banks, function(bank){ %>
        <div class="MortgageBanklist_row">
            <div class="MortgageBanklist_cell MortgageBanklist_cell-img"><img src="<%= bank.logo %>" title="<%= bank.name %>" alt="ЛОГО <%= bank.name %>"></div>
            <div class="MortgageBanklist_cell MortgageBanklist_cell-description">
                <h4 class="MortgageBanklist_header"><span class="hidden-sm">Первоначальный </span>взнос</h4>
                <p class="MortgageBanklist_text">
                    <% if(bank.firstFrom === bank.firstTo) {%>
                        <%= bank.firstFrom %>
                    <%}else if(bank.firstTo === 100){%>
                        от <%= bank.firstFrom %>
                    <%}else{%>
                        <%= bank.firstFrom %> - <%= bank.firstTo %>
                    <%};%>%
                </p>
            </div>
            <div class="MortgageBanklist_cell MortgageBanklist_cell-rate">
                    <% if(bank.isHinted) {%>
                        <h4 class="MortgageBanklist_header MortgageBanklist_header-star">Ставка</h4>
                    <%}else{%>
                        <h4 class="MortgageBanklist_header">Ставка</h4>
                    <%};%>
                <p class="MortgageBanklist_text" style="text-align:center;">
                    <% if(bank.rateFrom === bank.rateTo) {%>
                        <%= bank.rateFrom %>
                    <%}else{%>
                        <%= bank.rateFrom %> - <%= bank.rateTo %>
                    <%};%>%
            </p>
            </div>
            <div class="MortgageBanklist_cell">
                <h4 class="MortgageBanklist_header">Срок кредита до</h4>
                <p class="MortgageBanklist_text"><%= bank.yearsTo %> лет</p>
            </div>
            <div class="MortgageBanklist_cell">
                <h4 class="MortgageBanklist_header">Платежи в месяц</h4>
                <p class="MortgageBanklist_text"><%= bank.monthlyPayment %> ₽</p>
            </div>
        </div>
        <%}); %>
        <p class="MortgageBanklist_tipText"><span class="MortgageBanklist_tipSign">*</span>подробности предоставления данных условий уточняйте у менеджера по ипотеке.</p>
</div>